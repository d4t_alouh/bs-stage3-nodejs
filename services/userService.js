const {UserRepository} = require('../repositories/userRepository');

class UserService {

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    findOne(id) {
        const user = this.search({id})
        if (!user) {
            throw Error("User not found")
        }
        return user
    }

    getAll() {
        return UserRepository.getAll()
    }

    create(data) {
        const emailUser = this.search({email: data.email})
        if (emailUser) {
            throw Error("User with this email already exists")
        }
        const phoneUser = this.search({phoneNumber: data.phoneNumber})
        if (phoneUser) {
            throw Error("User with this phone number already exists")
        }
        return UserRepository.create(data)
    }

    update(id, data) {
        const user = UserRepository.update(id, data)
        if (!user) {
            throw Error("User not found")
        }
        return user
    }

    delete(id) {
        const user = UserRepository.delete({id})
        if (!user) {
            throw Error("User not found")
        }
        return user
    }
}

module.exports = new UserService();
